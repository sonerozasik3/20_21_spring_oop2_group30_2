﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryApp
{
    public partial class Shopping : Form
    {
        static string username = "";     
        productList plist = productList.Instance;
        public Shopping()
        {
            InitializeComponent();
        }
        public Shopping(string uname)
        {
            InitializeComponent();
            username = uname; 
        }

        private void Shopping_Load(object sender, EventArgs e)
        {
            Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ShoppingCart sp = new ShoppingCart(username);
            sp.Show();
            this.Hide();
        }

        private void dgv_frontstore_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
            UserPanel usp = new UserPanel(username,int.Parse(dgv_frontstore.SelectedRows[0].Cells[0].Value.ToString()));
            this.Hide();
            usp.Show();
        }
        public void Refresh()
        {
            dgv_frontstore.Columns.Clear();
            dgv_frontstore.EnableHeadersVisualStyles = false;
            dgv_frontstore.Columns.Add("id", "id");
            dgv_frontstore.Columns.Add("Name", "Name");
            dgv_frontstore.Columns.Add("Price", "Price");
            dgv_frontstore.Columns.Add("Description", "Description");
            dgv_frontstore.Columns.Add("Stock", "Stock");
            DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
            imgcolumn.Name = "Image";
            imgcolumn.HeaderText = "Product";
            dgv_frontstore.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dgv_frontstore.Columns.Add(imgcolumn);
            DataGridViewRow dgvrow = dgv_frontstore.RowTemplate;
            dgvrow.Height = 90;
            imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
            plist.updateProductsFromSql();
            dgv_frontstore.Columns[0].Visible = false;
            plist.printProductsToDgv(dgv_frontstore);
        }

        private void btn_myprofile_Click(object sender, EventArgs e)
        {
            UserConfig userConfig = UserConfig.getInstance();
            userConfig.getUsersfromDB();
            Myprofile mp = new Myprofile(username);
            mp.Show();
            this.Hide();
        }

        private void Shopping_FormClosed(object sender, FormClosedEventArgs e)
        {
            Login log = new Login();
            log.Show();
            this.Hide();
        }
    }
}

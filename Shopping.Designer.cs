﻿
namespace GroceryApp
{
    partial class Shopping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dgv_frontstore = new System.Windows.Forms.DataGridView();
            this.btn_myprofile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_frontstore)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(861, 482);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 60);
            this.button1.TabIndex = 20;
            this.button1.Text = "Sign Out";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(34, 482);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 60);
            this.button2.TabIndex = 21;
            this.button2.Text = "Open Shopping Cart";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dgv_frontstore
            // 
            this.dgv_frontstore.AllowUserToAddRows = false;
            this.dgv_frontstore.AllowUserToDeleteRows = false;
            this.dgv_frontstore.AllowUserToResizeColumns = false;
            this.dgv_frontstore.AllowUserToResizeRows = false;
            this.dgv_frontstore.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_frontstore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_frontstore.Location = new System.Drawing.Point(34, 12);
            this.dgv_frontstore.MultiSelect = false;
            this.dgv_frontstore.Name = "dgv_frontstore";
            this.dgv_frontstore.ReadOnly = true;
            this.dgv_frontstore.RowHeadersVisible = false;
            this.dgv_frontstore.RowTemplate.Height = 24;
            this.dgv_frontstore.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_frontstore.Size = new System.Drawing.Size(947, 445);
            this.dgv_frontstore.TabIndex = 22;
            this.dgv_frontstore.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_frontstore_CellMouseDoubleClick);
            // 
            // btn_myprofile
            // 
            this.btn_myprofile.Location = new System.Drawing.Point(178, 482);
            this.btn_myprofile.Name = "btn_myprofile";
            this.btn_myprofile.Size = new System.Drawing.Size(120, 60);
            this.btn_myprofile.TabIndex = 23;
            this.btn_myprofile.Text = "My Profile";
            this.btn_myprofile.UseVisualStyleBackColor = true;
            this.btn_myprofile.Click += new System.EventHandler(this.btn_myprofile_Click);
            // 
            // Shopping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 555);
            this.Controls.Add(this.btn_myprofile);
            this.Controls.Add(this.dgv_frontstore);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Shopping";
            this.Text = "Shopping";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Shopping_FormClosed);
            this.Load += new System.EventHandler(this.Shopping_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_frontstore)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dgv_frontstore;
        private System.Windows.Forms.Button btn_myprofile;
    }
}
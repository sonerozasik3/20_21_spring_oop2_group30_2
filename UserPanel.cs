﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryApp
{
    public partial class UserPanel : Form
    {
        static string username = "";
        productList plist = productList.Instance;
        int product_id;
        shoppingCart sc;

        public UserPanel()
        {
            InitializeComponent();
        }

        public UserPanel(string uname, int product_id)
        {
            UserConfig uc = UserConfig.getInstance();
            InitializeComponent();
            username = uname;
            this.product_id = product_id;
            sc = new shoppingCart(uc.getUserId(uname));
        }

        private void UserPanel_Load(object sender, EventArgs e)
        {
            string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", plist.at(product_id).ImageFileName);
            itemImage.Image = Image.FromFile(path);

            TextBox_name2.Text = plist.at(product_id).Name;
            TextBox_description2.Text = plist.at(product_id).Description;
            TextBox_price2.Text = plist.at(product_id).Price.ToString();
            txt_amount.Text = plist.at(product_id).Amount.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            Shopping shopping = new Shopping(username);
            shopping.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (int.Parse(txt_amount.Text) > 0 && int.Parse(TextBox_amount.Text)<= int.Parse(txt_amount.Text))
            {
                itemToPurchase item = new itemToPurchase(int.Parse(TextBox_amount.Text), plist.at(product_id));
                item.Product.Amount -= int.Parse(TextBox_amount.Text);
                txt_amount.Text = (int.Parse(txt_amount.Text) - int.Parse(TextBox_amount.Text)).ToString();
                sc.AddProduct(item);
                plist.UploadRepositoryToDB();
            }
            else
            {
                MessageBox.Show("You cannot buy this amount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }
    }
}

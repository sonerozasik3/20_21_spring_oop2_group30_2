﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryApp
{
    public partial class Login : Form
    {
        UserConfig user = UserConfig.getInstance();
        SignUp sign = new SignUp();
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (userTb.Text == "")
            {
                lbResult.ForeColor = Color.Red;
                lbResult.Text = "Missing Information!\nPlease enter your username!";

            }
            else if (passTb.Text == "")
            {
                lbResult.ForeColor = Color.Red;
                lbResult.Text = "Missing Information!\nPlease enter your password!";
            }
            else
            {
                int check = user.verify(userTb.Text, passTb.Text);

                if (check == -1)
                {
                    lbResult.ForeColor = Color.Red;
                    lbResult.Text = "No such user exist!";
                    userTb.Text = "";
                    passTb.Text = "";
                }
                else if (check == 0)
                {
                    lbResult.ForeColor = Color.Red;
                    lbResult.Text = "Wrong password!";
                    userTb.Text = "";
                    passTb.Text = "";
                }
                else if (check == 1)
                {
                    lbResult.ForeColor = Color.Green;
                    lbResult.Text = "Successful Login!";
                    string userNameTemp = userTb.Text;
                    userTb.Text = "";
                    passTb.Text = "";
                    Application.DoEvents();
                    //Thread.Sleep(3000);
                    productList plist = productList.Instance;
                    plist.updateProductsFromSql();
                    plist.UpdateImagesToDB();
                    user.getUsersfromDB();
                    if (user.isAdmin(userNameTemp))
                    {
                         Form1 admin = new Form1();
                         admin.Show();
                         this.Hide();
                    }
                    else
                    {
                        Shopping shopping = new Shopping(userNameTemp);
                        shopping.Show();
                        this.Hide();
                    }
                }
            }
        }

        private void btn_SignUp_Click(object sender, EventArgs e)
        {
            sign.Show();
            this.Hide();
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}

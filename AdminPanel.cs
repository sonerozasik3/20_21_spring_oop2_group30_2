﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GroceryApp
{
    
    public partial class Form1 : Form
    {
        productList plist = productList.Instance;
        int last_id = 10;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            RefreshAdminPanel();

        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            double number;
            if(TextBox_name.Text == "" || TextBox_price.Text == "" || TextBox_description.Text == "" || lbl_image.Text == "" || txt_Amount.Text == "")
            {
                MessageBox.Show("Fields can't be empty when adding new item!", "Error!",  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(!double.TryParse(TextBox_price.Text, out number))
            {
                MessageBox.Show("Price must be a number!", "Error!",  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", lbl_image.Text);
                Product product = new Product(TextBox_name.Text,double.Parse(TextBox_price.Text),TextBox_description.Text, lbl_image.Text , path, int.Parse(txt_Amount.Text) , last_id);
                plist.add(product);                

                RefreshAdminPanel();

                last_id++;
            }
            TextBox_description.Text = "";
            TextBox_name.Text = "";
            TextBox_price.Text = "";
            lbl_image.Text = "";
            txt_Amount.Text = "";
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();

                string fileName = "";

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    fileName = ofd.SafeFileName;
                }

                lbl_image.Text = fileName;
                string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);               
            }
            catch(Exception ex)
            {
                MessageBox.Show("You didn't choose an image", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            double number;
            if (dgvAdmin.SelectedRows.Count < 0)
            {
                MessageBox.Show("You must select an item!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                if (TextBox_name.Text == "" || TextBox_price.Text == "" || TextBox_description.Text == "" || lbl_image.Text == ""||txt_Amount.Text == "")
                {
                    MessageBox.Show("Fields can't be empty when updating item!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!double.TryParse(TextBox_price.Text, out number))
                {
                    MessageBox.Show("Price must be a number!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", lbl_image.Text);

                    Product product = new Product(TextBox_name.Text, 
                        double.Parse(TextBox_price.Text), 
                        TextBox_description.Text, 
                        lbl_image.Text, path, 
                        int.Parse(txt_Amount.Text), 
                        dgvAdmin.CurrentRow.Index);

                    plist.update(product, dgvAdmin.CurrentRow.Index);
                    RefreshAdminPanel();
                }
            }
            TextBox_description.Text = "";
            TextBox_name.Text = "";
            TextBox_price.Text = "";
            lbl_image.Text = "";
            txt_Amount.Text = "";
            
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            TextBox_description.Text = "";
            TextBox_name.Text = "";
            TextBox_price.Text = "";
            lbl_image.Text = "";
            txt_Amount.Text = "";
            
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (dgvAdmin.SelectedRows.Count < 0)
            {
                MessageBox.Show("You must select an item!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvAdmin.Rows.RemoveAt(dgvAdmin.CurrentRow.Index);
                plist.delete(dgvAdmin.CurrentRow.Index);

                TextBox_description.Text = "";
                TextBox_name.Text = "";
                TextBox_price.Text = "";
                lbl_image.Text = "";
                txt_Amount.Text = "";
            }
        }

        private void RefreshAdminPanel()
        {

            dgvAdmin.Columns.Clear();
            dgvAdmin.EnableHeadersVisualStyles = false;
            dgvAdmin.Columns.Add("id", "id");
            dgvAdmin.Columns.Add("Name", "Name");
            dgvAdmin.Columns.Add("Price", "Price");
            dgvAdmin.Columns.Add("Description", "Description");
            dgvAdmin.Columns.Add("Stock", "Stock");
            DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
            imgcolumn.Name = "Image";
            imgcolumn.HeaderText = "Product";
            dgvAdmin.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dgvAdmin.Columns.Add(imgcolumn);
            DataGridViewRow dgvrow = dgvAdmin.RowTemplate;
            dgvrow.Height = 90;
            imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
            plist.updateProductsFromSql();
            dgvAdmin.Columns[0].Visible = false;
            plist.printProductsToDgv(dgvAdmin);
        }
        
        private void btn_signout_Click(object sender, EventArgs e)
        {
            plist.UploadRepositoryToDB();
            this.Close();
            Login login = new Login();
            login.Show();
        }

        private void dgvAdmin_SelectionChanged(object sender, EventArgs e)
        {
            TextBox_name.Text = dgvAdmin.CurrentRow.Cells[1].Value.ToString();
            TextBox_price.Text = dgvAdmin.CurrentRow.Cells[2].Value.ToString();
            TextBox_description.Text = dgvAdmin.CurrentRow.Cells[3].Value.ToString();
            txt_Amount.Text = dgvAdmin.CurrentRow.Cells[4].Value.ToString();
            lbl_image.Text = plist.at(int.Parse(dgvAdmin.CurrentRow.Cells[0].Value.ToString())).ImageFileName.ToString();
        }
    }
}

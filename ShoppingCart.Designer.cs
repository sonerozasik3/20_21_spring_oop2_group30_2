﻿
namespace GroceryApp
{
    partial class ShoppingCart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_change = new System.Windows.Forms.Button();
            this.txt_amount = new System.Windows.Forms.TextBox();
            this.btn_remove = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_totalprice = new System.Windows.Forms.Label();
            this.txt_totalprice = new System.Windows.Forms.TextBox();
            this.dgv_sc = new System.Windows.Forms.DataGridView();
            this.btnBuy = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sc)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_change
            // 
            this.btn_change.Location = new System.Drawing.Point(29, 96);
            this.btn_change.Name = "btn_change";
            this.btn_change.Size = new System.Drawing.Size(121, 43);
            this.btn_change.TabIndex = 1;
            this.btn_change.Text = "Change Amount";
            this.btn_change.UseVisualStyleBackColor = true;
            this.btn_change.Click += new System.EventHandler(this.btn_change_Click);
            // 
            // txt_amount
            // 
            this.txt_amount.Location = new System.Drawing.Point(29, 55);
            this.txt_amount.Name = "txt_amount";
            this.txt_amount.Size = new System.Drawing.Size(121, 22);
            this.txt_amount.TabIndex = 0;
            // 
            // btn_remove
            // 
            this.btn_remove.Location = new System.Drawing.Point(29, 157);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(121, 43);
            this.btn_remove.TabIndex = 2;
            this.btn_remove.Text = "Remove Product";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Product Amount";
            // 
            // lbl_totalprice
            // 
            this.lbl_totalprice.AutoSize = true;
            this.lbl_totalprice.Location = new System.Drawing.Point(12, 292);
            this.lbl_totalprice.Name = "lbl_totalprice";
            this.lbl_totalprice.Size = new System.Drawing.Size(84, 17);
            this.lbl_totalprice.TabIndex = 6;
            this.lbl_totalprice.Text = "Total Price :";
            // 
            // txt_totalprice
            // 
            this.txt_totalprice.Location = new System.Drawing.Point(102, 292);
            this.txt_totalprice.Name = "txt_totalprice";
            this.txt_totalprice.ReadOnly = true;
            this.txt_totalprice.Size = new System.Drawing.Size(70, 22);
            this.txt_totalprice.TabIndex = 7;
            // 
            // dgv_sc
            // 
            this.dgv_sc.AllowUserToAddRows = false;
            this.dgv_sc.AllowUserToDeleteRows = false;
            this.dgv_sc.AllowUserToOrderColumns = true;
            this.dgv_sc.AllowUserToResizeColumns = false;
            this.dgv_sc.AllowUserToResizeRows = false;
            this.dgv_sc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_sc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_sc.Location = new System.Drawing.Point(175, 55);
            this.dgv_sc.Name = "dgv_sc";
            this.dgv_sc.ReadOnly = true;
            this.dgv_sc.RowHeadersWidth = 51;
            this.dgv_sc.RowTemplate.Height = 24;
            this.dgv_sc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_sc.Size = new System.Drawing.Size(743, 332);
            this.dgv_sc.TabIndex = 8;
           
            // 
            // btnBuy
            // 
            this.btnBuy.Location = new System.Drawing.Point(29, 223);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(121, 43);
            this.btnBuy.TabIndex = 9;
            this.btnBuy.Text = "Buy";
            this.btnBuy.UseVisualStyleBackColor = true;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // ShoppingCart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 417);
            this.Controls.Add(this.btnBuy);
            this.Controls.Add(this.dgv_sc);
            this.Controls.Add(this.txt_totalprice);
            this.Controls.Add(this.lbl_totalprice);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.txt_amount);
            this.Controls.Add(this.btn_change);
            this.Name = "ShoppingCart";
            this.Text = "ShoppingCart";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ShoppingCart_FormClosed);
            this.Load += new System.EventHandler(this.ShoppingCart_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_sc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_change;
        private System.Windows.Forms.TextBox txt_amount;
        private System.Windows.Forms.Button btn_remove;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_totalprice;
        private System.Windows.Forms.TextBox txt_totalprice;
        private System.Windows.Forms.DataGridView dgv_sc;
        private System.Windows.Forms.Button btnBuy;
    }
}
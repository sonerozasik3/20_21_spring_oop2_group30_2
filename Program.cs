﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.IO;

namespace GroceryApp
{
    static class Program
    {
        /// <summary>
        /// Uygulamanın ana girdi noktası.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
        }
    }

    // Güncellenecek veya silinecek
    public class Product
    {
        private string name;
        private double price;
        private string description;
        private string imageFileName;
        private string imagepath;
        private int amount;
        private int id;

        public Product(string name, double price, string description, string imageFile , string imagepath, int amount , int id)
        {
            this.name = name;
            this.price = price;
            this.description = description;
            this.imageFileName = imageFile;
            this.imagepath = imagepath;
            this.amount = amount;
            this.id = id;
        }


        public Product(){}

        public string Name {
            get { return name; }
            set { name = value; }
        }

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string ImageFileName
        {
            get { return imageFileName; }
            set { imageFileName = value; }
        }

        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string ImagePath
        {
            get { return imagepath; }
            set { imagepath = value; }
        }
    }

    // Güncellenecek , Fonksyionlar iş görür
    public class productList
    {
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;

        private List<Product> products = new List<Product>();

        private productList() { }

        private static productList instance = null;
        public static productList Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new productList();
                }
                return instance;
            }
        }

        public void add(Product item)
        {
            products.Add(item);
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            command = new SqlCommand("INSERT INTO Products (Name, Price, Description, Stock, ImageName, ImagePath) " + "VALUES (@N, @P, @D , @S, @IN, @IP)", connection);

            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@TP", item.Name);
            command.Parameters.AddWithValue("@TP", item.Price);
            command.Parameters.AddWithValue("@D", item.Description);
            command.Parameters.AddWithValue("@S", item.Amount);
            command.Parameters.AddWithValue("@IN", item.ImagePath);
            command.Parameters.AddWithValue("@IP", item.ImageFileName);
            command.ExecuteNonQuery();
            connection.Close();
        }

        public void delete(int index)
        {
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();

            command = new SqlCommand("DELETE FROM ShoppingCart WHERE Products_id = @Products_id", connection);
            command.Parameters.AddWithValue("@Products_id", products[index].Id);
            command.ExecuteNonQuery();

            command = new SqlCommand("DELETE FROM Products WHERE Products_id = @Products_id", connection);
            command.Parameters.AddWithValue("@Products_id", products[index].Id);
            command.ExecuteNonQuery();
            products.RemoveAt(index);

            connection.Close();
        }

        public void update(Product item, int index)
        {
            products[index].Name = item.Name;
            products[index].Price = item.Price;
            products[index].Description = item.Description;
            products[index].ImageFileName = item.ImageFileName;
            products[index].ImagePath = item.ImagePath;
            products[index].Amount = item.Amount;
            UploadRepositoryToDB();
        }


        public Product at(int id)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (products[i].Id == id)
                {
                    return products[i];
                }
            }
            return null;
        }

        public int index(Product product)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (product.Name == products[i].Name)
                {
                    return i;
                }
            }
            return -1;
        }

        public void updateProductsFromSql()
        {
            products.Clear();
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();

            da = new SqlDataAdapter("SELECT *FROM Products", connection);
            DataTable table = new DataTable();
            da.Fill(table);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                Product product = new Product(row.Field<string>("Name"), (double)row.Field<decimal>("Price"), row.Field<string>("Description"), row.Field<string>("ImageName"), row.Field<string>("ImagePath"), row.Field<int>("Stock"), row.Field<int>("Products_id"));//to do
                products.Add(product);
            }
        }

        public void printProductsToDgv(DataGridView dgv)
        {
            for (int i = 0; i < products.Count; i++)
            {
                Image img = Image.FromFile(products[i].ImagePath);
                dgv.Rows.Add(products[i].Id, products[i].Name, products[i].Price, products[i].Description, products[i].Amount, img);
            }
        }

        public void UploadRepositoryToDB()
        {
            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            for (int i = 0; i < products.Count; i++)
            {
                SqlCommand cmd = new SqlCommand("UPDATE Products SET Name = @Name, Price = @Price, Description = @Description, Stock = @Stock, ImageName = @ImageName, ImagePath = @ImagePath WHERE Products_id = @Products_id", connection);
                cmd.Parameters.AddWithValue("@Name", products[i].Name);
                cmd.Parameters.AddWithValue("@Price", products[i].Price);
                cmd.Parameters.AddWithValue("@Description", products[i].Description);
                cmd.Parameters.AddWithValue("@Stock", products[i].Amount);
                cmd.Parameters.AddWithValue("@ImageName", products[i].ImageFileName);
                cmd.Parameters.AddWithValue("@Products_id", products[i].Id);
                cmd.Parameters.AddWithValue("@ImagePath", products[i].ImagePath);
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateImagesToDB()
        {
            for (int i = 0; i < products.Count; i++)
            {
                string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", products[i].ImageFileName);
                products[i].ImagePath = path;
                SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
                connection.Open();
                SqlCommand cmd = new SqlCommand("UPDATE Products SET ImagePath = @ImagePath WHERE Products_id = @Products_id", connection);
                cmd.Parameters.AddWithValue("@Products_id", products[i].Id);
                cmd.Parameters.AddWithValue("@ImagePath", products[i].ImagePath);
                cmd.ExecuteNonQuery();
                //updateProductsFromSql();    // TODO SİLİNEBİLİR
            }
        }
    }

    public abstract class Observer
    {
        public abstract void update(Product item , int state);
    }

    // Güncellenecek veya Silinecek
    //public class shoppingcard: Observer
    //{
    //    private List<Product> carditems = new List<Product>();
    //    productList prlist = productList.Instance;

    //    public override void update(Product product , int state )
    //    {
    //        if (state == 0)
    //        {
    //            for(int i = 0; i < carditems.Count; i++)
    //            {
    //                if(carditems[i].Id == product.Id)
    //                {
    //                    carditems[i].Name = product.Name;
    //                    carditems[i].Description = product.Description;
    //                    carditems[i].Price = product.Price;
    //                    carditems[i].ImageFileName = product.ImageFileName;
    //                    break;
    //                }
    //            }
    //        }

    //        else if (state == 1)
    //        {
    //            int temp = prlist.at(prlist.index(product)).Amount - product.Amount;
    //            if ( temp >= 0)
    //            {
    //                bool flag = false;
    //                for(int i = 0; i < carditems.Count; i++)
    //                {
    //                    if (carditems[i].Name == product.Name)
    //                    {
    //                        flag = true;
    //                        carditems[i].Amount += product.Amount;
    //                        break;
    //                    }
    //                }
    //                if(flag == false)   carditems.Add(product);
    //            }
    //        }

    //        else if(state == 2)
    //        {
    //            for (int i = 0; i < carditems.Count; i++)
    //            {
    //                if (carditems[i].Name == product.Name)
    //                {
    //                    if (carditems[i].Amount < product.Amount)
    //                    {
    //                        int temp = carditems[i].Amount;

    //                        if (prlist.at(prlist.index(carditems[i])).Amount >= (product.Amount - carditems[i].Amount))
    //                        {
    //                            carditems[i].Amount = product.Amount;

    //                        }
    //                        else
    //                        {
    //                            MessageBox.Show("You Can't Buy This Amount of Items", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
    //                        }
    //                        product.Amount = (-1) * (product.Amount - temp);

    //                    }
    //                    else
    //                    {
    //                        int temp=carditems[i].Amount;
    //                        carditems[i].Amount = product.Amount;
    //                        product.Amount = (-1) * (product.Amount - temp);

    //                    }

    //                }
    //            }
    //        }
    //        else if (state == 3)
    //        {
    //            for (int i = 0; i < carditems.Count; i++)
    //            {
    //                if (carditems[i].Name == product.Name)
    //                {
    //                    carditems.RemoveAt(i);
    //                    break;
    //                }
    //            }

    //        }
    //    }

    //    public List<Product> getProducts()
    //    {
    //        return carditems;
    //    }
    //}

    public class itemToPurchase
    {
        int amount;
        Product product;

        public int Amount
        {
            get { return amount; }
            set{amount = value;}
        }

        public Product Product
        {
            get { return product; }
            set { product = value; }
        }
        public itemToPurchase(int amount,Product product)
        {
            this.amount = amount;
            this.product = product;
        }
    } 
    public class shoppingCart
    {
        List<itemToPurchase> items = new List<itemToPurchase>();
        productList pl = productList.Instance;
        int userId;

        public shoppingCart(int userId)
        {
            this.userId = userId;
            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            SqlCommand command = new SqlCommand("SELECT *FROM ShoppingCart WHERE Users_id LIKE '" + userId + "'", connection);
            command.ExecuteNonQuery();
            if (command.ExecuteScalar() != null)
            {
                DataTable table = new DataTable();
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    table.Load(dr);
                }
                for(int i = 0; i < table.Rows.Count; i++)
                {
                    DataRow row = table.Rows[i];
                    itemToPurchase itp = new itemToPurchase(row.Field<int>("Amount"), pl.at(row.Field<int>("Products_id")));
                    items.Add(itp);
                }
            }
        }
        public void printProductsToDgv(DataGridView dgv)
        {
            for (int i = 0; i < items.Count; i++)
            {
                string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", items[i].Product.ImageFileName);
                Image img = Image.FromFile(path);
                dgv.Rows.Add(items[i].Product.Id, items[i].Product.Name, items[i].Product.Price, items[i].Amount, img);
            }
        }

        public void changeAmountofProduct(int productId,int amount)
        {
            for(int i= 0; i < items.Count; i++)
            {
                if (items[i].Product.Id == productId)
                {
                    items[i].Amount = amount;
                    UploadShoppingCartToDB();
                    break;
                }
            }
        }

        public void RemoveProduct(int index)
        {       

            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();            
            
            SqlCommand cmd = new SqlCommand("DELETE FROM ShoppingCart WHERE Products_id = @Products_id AND Users_id = @Users_id", connection);           
            cmd.Parameters.AddWithValue("@Products_id", items[index].Product.Id);
            cmd.Parameters.AddWithValue("@Users_id", userId);
            cmd.ExecuteNonQuery();

            pl.at(items[index].Product.Id).Amount += items[index].Amount;
            pl.UploadRepositoryToDB();

            items.RemoveAt(index);
            connection.Close();
        }

        public void CompletePurchase()
        {
            items.Clear();
            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();

            SqlCommand cmd = new SqlCommand("DELETE FROM ShoppingCart WHERE Users_id = @Users_id", connection);
            cmd.Parameters.AddWithValue("@Users_id", userId);
            cmd.ExecuteNonQuery();
        }
        public void UploadShoppingCartToDB()
        {
            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            for (int i = 0; i < items.Count; i++)
            {

                SqlCommand cmd = new SqlCommand("UPDATE ShoppingCart SET Amount = @Amount WHERE Products_id = @Products_id AND Users_id = @Users_id", connection);
                cmd.Parameters.AddWithValue("@Amount", items[i].Amount);
                cmd.Parameters.AddWithValue("@Products_id", items[i].Product.Id );
                cmd.Parameters.AddWithValue("@Users_id",userId);
                cmd.ExecuteNonQuery();
            }
        }

        public void AddProduct(itemToPurchase item)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Product.Id == item.Product.Id)
                {
                    items[i].Amount += item.Amount;
                    UploadShoppingCartToDB();
                    return;
                }
            }
            items.Add(item);
            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            SqlCommand command = new SqlCommand("INSERT INTO ShoppingCart (Users_id, Products_id, Amount) VALUES (@UI, @PI, @A)", connection);

            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@UI", userId);
            command.Parameters.AddWithValue("@PI", item.Product.Id);
            command.Parameters.AddWithValue("@A", item.Amount);
            command.ExecuteNonQuery();
            connection.Close();

        }

        public void deleteshoppingcart()
        {
            for(int i = 0; i < items.Count;i++)
            {
                pl.at(items[i].Product.Id).Amount += items[i].Amount;
                SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
                connection.Open();

                SqlCommand cmd = new SqlCommand("DELETE FROM ShoppingCart WHERE Products_id = @Products_id AND Users_id = @Users_id", connection);
                cmd.Parameters.AddWithValue("@Products_id", items[i].Product.Id);
                cmd.Parameters.AddWithValue("@Users_id", userId);
                cmd.ExecuteNonQuery();
                pl.UploadRepositoryToDB();
            }
            items.Clear();
        }

    }
    // Sql e bağlanacak
    /*public class stocklist : Observer
    {
        productList prlist = productList.Instance;

        private stocklist() { }

        private static stocklist instance = null;
        public static stocklist Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new stocklist();
                }
                return instance;
            }
        }
        public override void update(Product product, int state)
        {
            if (state == 0)
            {
                prlist.update(product , prlist.index(product));
            }

            else if(state == 1)
            {
                int temp = prlist.at(prlist.index(product)).Amount - product.Amount;
                if(temp < 0)
                {
                    MessageBox.Show("You Can't Buy This Amount of Items", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    prlist.at(prlist.index(product)).Amount = temp;
                }
            }

            else if (state == 2)
            {
                if ((-1)*(product.Amount) <= prlist.at(prlist.index(product)).Amount)
                {
                    int temp = prlist.at(prlist.index(product)).Amount + product.Amount;
                    product.Amount = temp;
                    prlist.update(product, prlist.index(product));
                }               
            }
            else if (state == 3)
            {
                prlist.at(prlist.index(product)).Amount += product.Amount;
            }
        }
    }*/

    // Güncellenecek
    /*public class Subject
    {
        private List<Observer> observers = new List<Observer>();

        private static Subject instance = null;

        private Subject() { }
        public static Subject Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Subject();
                }
                return instance;
            }
        }
        public void Attach(Observer obs)
        {
            observers.Add(obs);
        }

        public void Detach(Observer obs)
        {
            observers.Remove(obs);
        }

        private void Notify(Product item, int state)
        {
            for(int i = observers.Count-1 ; i >= 0; i--)
            {
                observers[i].update(item, state);
            }
        }

        public void Change(Product item, int state)
        {
            Notify(item, state);
        }

    }*/

    // Silinebilir
    public struct User
    {
        public string name;
        public string username;
        public string password;
        public string userType;
        public string address;
        public string phone_num;
        public string Image;
        public User(string name,string username,string password,string uT = "User" , string adress = "Unknown", string phonenum = "Unknown", string image = "Unknown") 
        { this.name = name; this.username = username; this.password = password; userType = uT; this.address = adress; this.phone_num = phonenum; this.Image = image; }
    };

     // Kalabilir
    public class UserConfig
    {
        SqlConnection connection;
        SqlCommand command;


        // Silinecek liste
        List<User> users = new List<User>();
        private UserConfig(){}
        private static UserConfig instance;
        public static UserConfig getInstance()
        {
            if (instance == null)
            {
                instance = new UserConfig();
            }
            return instance;
        }

        public void kullanici_ekle(string name, string username, string password,string type)
        {
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();

            command = new SqlCommand("INSERT INTO Users (Name, Username, Password, UserType) VALUES (@Name, @Username, @Password, @UT)", connection);

            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@Name", name);
            command.Parameters.AddWithValue("@Username", username);
            command.Parameters.AddWithValue("@Password", password);
            command.Parameters.AddWithValue("@UT", type);
            command.ExecuteNonQuery();
            connection.Close();

            User temp = new User(name, username, password, type);
            users.Add(temp);
        }

        public int verify(string n, string p)
        {
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            command = new SqlCommand("SELECT Password FROM Users WHERE Username LIKE '" + n + "'", connection);
            command.ExecuteNonQuery();

            if (command.ExecuteScalar() == null)
            {
                connection.Close();
                return -1;
            }
            else
            {
                if (command.ExecuteScalar().ToString() != p)
                {
                    connection.Close();
                    return 0;
                }
                else
                {
                    connection.Close();
                    return 1;
                }
            }
        }

        public bool checkUsername(string u)
        {
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            command = new SqlCommand("SELECT UserType FROM Users WHERE Username LIKE '" + u + "'", connection);
            command.ExecuteNonQuery();

            if (command.ExecuteScalar() == null)
            {
                connection.Close();
                return true;
            }
            else
            {
                connection.Close();
                return false;
            }
        }

        public bool isAdmin(string u)
        {
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            command = new SqlCommand("SELECT UserType FROM Users WHERE Username LIKE '" + u + "'", connection);
            command.ExecuteNonQuery();

            if(command.ExecuteScalar() == null)
            {
                connection.Close();
                return false;
            }
            else
            {
                if (command.ExecuteScalar().ToString() == "Admin")
                {
                    connection.Close();
                    return true;
                }
                else
                {
                    connection.Close();
                    return false;
                }
            }
        }

        public int getUserId(string userName)
        {
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
            command = new SqlCommand("SELECT Users_id FROM Users WHERE Username LIKE '" + userName + "'", connection);
            command.ExecuteNonQuery();
            if (command.ExecuteScalar() == null)
            {
                connection.Close();
                return -1;
            }
            else
            {   
                return int.Parse(command.ExecuteScalar().ToString());
            }
            connection.Close();
        }
        
        public void getUsersfromDB()
        {
            users.Clear();
            connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();

            SqlDataAdapter da = new SqlDataAdapter("SELECT *FROM Users", connection);
            DataTable table = new DataTable();
            da.Fill(table);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                User temp = new User(row.Field<string>("Name"), row.Field<string>("Username"), row.Field<string>("Password"), row.Field<string>("UserType"), row.Field<string>("Address"), row.Field<string>("PhoneNumber"), row.Field<string>("ImageName"));
                users.Add(temp);
            }
            connection.Close();
        }

        public User getUser(string username)
        {
            int i;
            for (i = 0; i < users.Count; i++)
            {
                if(users[i].username == username)
                {
                    return users[i];
                }
            }
            return users[i];
        }

        public void deleteUser(string username)
        {
            int i;
            for (i = 0; i < users.Count; i++)
            {
                if (users[i].username == username)
                {
                    users.RemoveAt(i);
                }
            }

            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();

            SqlCommand command = new SqlCommand("DELETE FROM Users WHERE Users_id = @UI", connection);
            command.Parameters.AddWithValue("@UI", getUserId(username));
            command.ExecuteNonQuery();
            connection.Close();
        }
    };
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace GroceryApp
{
    public partial class Myprofile : Form
    {
        UserConfig userConfig = UserConfig.getInstance();
        string username = "";
        int id;
        public Myprofile()
        {
            InitializeComponent();
        }

        public Myprofile(string uname)
        {
            InitializeComponent();
            username = uname;
            id = userConfig.getUserId(username);
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            userConfig.getUsersfromDB();
            Shopping shopping = new Shopping(txt_username.Text);
            shopping.Show();
            this.Hide();
        }

        private void Myprofile_Load(object sender, EventArgs e)
        {
            txt_name.Text = userConfig.getUser(username).name;
            txt_username.Text = userConfig.getUser(username).username;
            txt_password.Text = userConfig.getUser(username).password;
            txt_address.Text = userConfig.getUser(username).address;
            txt_phonenum.Text = userConfig.getUser(username).phone_num;

            if (userConfig.getUser(username).Image != null)
            {
                if (File.Exists(Path.Combine(Environment.CurrentDirectory, @"pictures\", userConfig.getUser(username).Image)))
                {
                    string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", userConfig.getUser(username).Image);
                    Image img = Image.FromFile(path);
                    pictureBox1.Image = img;
                }
                else
                {
                    MessageBox.Show("Your picture's name is changed or picture is deleted!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btn_updateinfo_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
            connection.Open();
           
            SqlCommand cmd = new SqlCommand("UPDATE Users SET Username = @UN, Password = @P, Address = @A, PhoneNumber = @PN WHERE Users_id = @Users_id", connection);
            cmd.Parameters.AddWithValue("@UN", txt_username.Text);
            cmd.Parameters.AddWithValue("@P", txt_password.Text);
            cmd.Parameters.AddWithValue("@A", txt_address.Text);
            cmd.Parameters.AddWithValue("@PN",txt_phonenum.Text);
            cmd.Parameters.AddWithValue("@Users_id", id);
            cmd.ExecuteNonQuery();
            userConfig.getUsersfromDB();
            username = txt_username.Text;
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            shoppingCart sc = new shoppingCart(userConfig.getUserId(username));
            sc.deleteshoppingcart();
            userConfig.deleteUser(username);
            Login log = new Login();
            log.Show();
            this.Hide();
        }

        private void btn_addpic_Click(object sender, EventArgs e)
        {
            if(userConfig.getUser(username).Image == null)
            {
                try
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    string fileName = "";

                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        fileName = ofd.SafeFileName;
                    }
                    string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
                    Image img = Image.FromFile(path);
                    pictureBox1.Image = img;

                    SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
                    connection.Open();

                    SqlCommand cmd = new SqlCommand("UPDATE Users SET ImageName = @IN WHERE Users_id = @UI", connection);
                    cmd.Parameters.AddWithValue("@UI", id);
                    cmd.Parameters.AddWithValue("@IN", fileName);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("You didn't choose an image", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private void btn_changepic_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                string fileName = "";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    fileName = ofd.SafeFileName;
                }
                string path = Path.Combine(Environment.CurrentDirectory, @"pictures\", fileName);
                Image img = Image.FromFile(path);
                pictureBox1.Image = img;

                SqlConnection connection = new SqlConnection("Data Source=SQL5063.site4now.net;Initial Catalog=db_a759c8_grocerystore;User Id=db_a759c8_grocerystore_admin;Password=GroceryStore123");
                connection.Open();

                SqlCommand cmd = new SqlCommand("UPDATE Users SET ImageName = @IN WHERE Users_id = @UI", connection);
                cmd.Parameters.AddWithValue("@UI", id);
                cmd.Parameters.AddWithValue("@IN", fileName);
                cmd.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("You didn't choose an image", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_signout_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryApp
{
  
    public partial class ShoppingCart : Form
    {
        bool isSelected = false;
        static string userName = "";
        shoppingCart sc = null;
        productList pl = productList.Instance;
        public ShoppingCart()
        {
            InitializeComponent();
        }

        public ShoppingCart(string username)
        {
            InitializeComponent();
            userName = username;
        }
        private void ShoppingCart_Load(object sender, EventArgs e)
        {
            RefreshDGV();
        }

        private void btn_change_Click(object sender, EventArgs e)
        {
           
                if (int.Parse(txt_amount.Text) > 0)
                {
                    sc.changeAmountofProduct(int.Parse(dgv_sc.SelectedRows[0].Cells[0].Value.ToString()), int.Parse(txt_amount.Text));
                    pl.at(int.Parse(dgv_sc.SelectedRows[0].Cells[0].Value.ToString())).Amount+= int.Parse(dgv_sc.SelectedRows[0].Cells[3].Value.ToString())-int.Parse(txt_amount.Text);
                    pl.UploadRepositoryToDB();
                    RefreshDGV();
                }
                else
                {
                    MessageBox.Show("Enter a valid number!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
           
            updateTotalPrice();
        }
        private void btn_remove_Click(object sender, EventArgs e)
        {
            if (dgv_sc.SelectedRows.Count > 0)
            {
                sc.RemoveProduct(dgv_sc.SelectedCells[0].RowIndex);
                RefreshDGV();
            }
            else
            {
                MessageBox.Show("Choose an element!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            txt_amount.Text = "";
            updateTotalPrice();
        }

        private void updateTotalPrice()
        {
            double sum = 0;
            for (int i = 0; i < dgv_sc.Rows.Count; i++)
            {
                sum += double.Parse(dgv_sc.Rows[i].Cells[2].Value.ToString()) * double.Parse(dgv_sc.Rows[i].Cells[3].Value.ToString());
            }
            txt_totalprice.Text = sum.ToString() + "  TL";
        }

        public void RefreshDGV()
        {
            dgv_sc.Rows.Clear();
            dgv_sc.Columns.Clear();
            UserConfig uc = UserConfig.getInstance();
            sc = new shoppingCart(uc.getUserId(userName));
            dgv_sc.Columns.Add("id", "id");
            dgv_sc.Columns.Add("Name", "Name");
            dgv_sc.Columns.Add("Price", "Price");
            dgv_sc.Columns.Add("Amount", "Amount");
            dgv_sc.EnableHeadersVisualStyles = false;
            DataGridViewImageColumn imgcolumn = new DataGridViewImageColumn();
            imgcolumn.Name = "Image";
            imgcolumn.HeaderText = "Product";
            dgv_sc.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dgv_sc.Columns.Add(imgcolumn);
            DataGridViewRow dgvrow = dgv_sc.RowTemplate;
            dgvrow.Height = 100;
            imgcolumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
            dgv_sc.Columns[0].Visible = false;
            sc.printProductsToDgv(dgv_sc);
            updateTotalPrice();
        }

        private void ShoppingCart_FormClosed(object sender, FormClosedEventArgs e)
        {
            Shopping shopping = new Shopping(userName);
            this.Hide();
            shopping.Show();
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            sc.CompletePurchase();
            RefreshDGV();
            MessageBox.Show("Your purchase is completed","Completed",MessageBoxButtons.OK,MessageBoxIcon.Information);
        }
    }
}

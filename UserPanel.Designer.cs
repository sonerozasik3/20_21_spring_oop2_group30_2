﻿
namespace GroceryApp
{
    partial class UserPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBox_price2 = new System.Windows.Forms.TextBox();
            this.TextBox_description2 = new System.Windows.Forms.TextBox();
            this.TextBox_name2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_amount = new System.Windows.Forms.TextBox();
            this.txt_amount = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_image = new System.Windows.Forms.Label();
            this.itemImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.itemImage)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 301);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 64);
            this.button1.TabIndex = 7;
            this.button1.Text = "Add To Shopping Cart";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(49, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 21);
            this.label3.TabIndex = 26;
            this.label3.Text = "Description :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(97, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "Price :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(27, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 21);
            this.label6.TabIndex = 24;
            this.label6.Text = "Product Name :  ";
            // 
            // TextBox_price2
            // 
            this.TextBox_price2.Location = new System.Drawing.Point(173, 102);
            this.TextBox_price2.Margin = new System.Windows.Forms.Padding(4);
            this.TextBox_price2.Name = "TextBox_price2";
            this.TextBox_price2.ReadOnly = true;
            this.TextBox_price2.Size = new System.Drawing.Size(132, 22);
            this.TextBox_price2.TabIndex = 2;
            // 
            // TextBox_description2
            // 
            this.TextBox_description2.Location = new System.Drawing.Point(172, 134);
            this.TextBox_description2.Margin = new System.Windows.Forms.Padding(4);
            this.TextBox_description2.Name = "TextBox_description2";
            this.TextBox_description2.ReadOnly = true;
            this.TextBox_description2.Size = new System.Drawing.Size(244, 22);
            this.TextBox_description2.TabIndex = 3;
            // 
            // TextBox_name2
            // 
            this.TextBox_name2.Location = new System.Drawing.Point(172, 70);
            this.TextBox_name2.Margin = new System.Windows.Forms.Padding(4);
            this.TextBox_name2.Name = "TextBox_name2";
            this.TextBox_name2.ReadOnly = true;
            this.TextBox_name2.Size = new System.Drawing.Size(132, 22);
            this.TextBox_name2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(27, 260);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 21);
            this.label1.TabIndex = 30;
            this.label1.Text = "Purchase Amount";
            // 
            // TextBox_amount
            // 
            this.TextBox_amount.Location = new System.Drawing.Point(172, 260);
            this.TextBox_amount.Margin = new System.Windows.Forms.Padding(4);
            this.TextBox_amount.Name = "TextBox_amount";
            this.TextBox_amount.Size = new System.Drawing.Size(132, 22);
            this.TextBox_amount.TabIndex = 5;
            // 
            // txt_amount
            // 
            this.txt_amount.Location = new System.Drawing.Point(173, 164);
            this.txt_amount.Margin = new System.Windows.Forms.Padding(4);
            this.txt_amount.Name = "txt_amount";
            this.txt_amount.ReadOnly = true;
            this.txt_amount.Size = new System.Drawing.Size(243, 22);
            this.txt_amount.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 12);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 42);
            this.button2.TabIndex = 0;
            this.button2.Text = "Back";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(70, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 21);
            this.label2.TabIndex = 33;
            this.label2.Text = "Amount :";
            // 
            // lbl_image
            // 
            this.lbl_image.AutoSize = true;
            this.lbl_image.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl_image.Location = new System.Drawing.Point(488, 344);
            this.lbl_image.Name = "lbl_image";
            this.lbl_image.Size = new System.Drawing.Size(0, 21);
            this.lbl_image.TabIndex = 36;
            // 
            // itemImage
            // 
            this.itemImage.Location = new System.Drawing.Point(492, 61);
            this.itemImage.Name = "itemImage";
            this.itemImage.Size = new System.Drawing.Size(287, 263);
            this.itemImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.itemImage.TabIndex = 39;
            this.itemImage.TabStop = false;
            // 
            // UserPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 436);
            this.Controls.Add(this.itemImage);
            this.Controls.Add(this.lbl_image);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txt_amount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBox_amount);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextBox_price2);
            this.Controls.Add(this.TextBox_description2);
            this.Controls.Add(this.TextBox_name2);
            this.Name = "UserPanel";
            this.Text = "UserPanel";
            this.Load += new System.EventHandler(this.UserPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.itemImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBox_price2;
        private System.Windows.Forms.TextBox TextBox_description2;
        private System.Windows.Forms.TextBox TextBox_name2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_amount;
        private System.Windows.Forms.TextBox txt_amount;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_image;
        private System.Windows.Forms.PictureBox itemImage;
    }
}
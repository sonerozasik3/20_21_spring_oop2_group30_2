﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryApp
{
   
    public partial class SignUp : Form
    {
        UserConfig user = UserConfig.getInstance();
        public SignUp()
        {
            InitializeComponent();
        }

        private void btn_signup_Click(object sender, EventArgs e)
        {
            if(name_txt.Text == "")
            {
                warningText.Text = "Please enter a name!"; warningText.ForeColor = Color.Red;
            }
            else
            {
                bool flag = true;
                if (username_txt.Text.Length < 3) { warningText.Text = "Your username must be at least 3 characters!"; warningText.ForeColor = Color.Red; flag = false; }
                else if (pass_txt.Text.Length < 6) { warningText.Text = "Your password must be at least 6 characters!"; warningText.ForeColor = Color.Red; flag = false; }
                else if (confirm_txt.Text == "") { warningText.Text = "Please confirm your password!"; warningText.ForeColor = Color.Red; flag = false; }
                else
                {
                    if (checkSpace(username_txt.Text) || checkSpace(pass_txt.Text)) { warningText.Text = "You can't use white space character!"; warningText.ForeColor = Color.Red; }
                    else
                    {
                        if (!user.checkUsername(username_txt.Text)) { warningText.Text = "This Username " + username_txt.Text + " is already taken!"; warningText.ForeColor = Color.Red; flag = false; }
                        else
                        {
                            if (pass_txt.Text != confirm_txt.Text) { warningText.Text = "Passwords does not match!"; warningText.ForeColor = Color.Red; flag = false; }
                            else
                            {
                                Login login = new Login();
                                warningText.Text = "Succesfully!"; warningText.ForeColor = Color.Green; flag = false;
                                user.kullanici_ekle(name_txt.Text, username_txt.Text, pass_txt.Text, "User");   // TODO ADDRESS VE PHONE NUMBER!
                                Application.DoEvents();
                                Thread.Sleep(2000);
                                login.Show();
                                this.Hide();
                            }
                        }
                    }
                }
            }
        }
        public bool checkSpace(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == ' ')
                {
                    return true;
                }
            }
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            this.Hide();
        }
    }
}
